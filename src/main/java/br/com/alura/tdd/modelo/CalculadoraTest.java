package br.com.alura.tdd.modelo;

import org.junit.Assert;
import org.junit.Test;

public class CalculadoraTest {

    @Test
    public void deveriaSomarDoisNumerosPositivos(){
        Calculadora calc = new Calculadora();
        int soma = calc.somar(3,7);

        // mesma coisa que eu mandar imprimir o valor em um system.out.
        // no entanto, eu peço para o JUnit verificar pra mim
        // informando que eu espero receber o valor 10 da variável soma.
        Assert.assertEquals(10, soma);
    }

}
