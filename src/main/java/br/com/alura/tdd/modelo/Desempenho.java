package br.com.alura.tdd.modelo;

import java.math.BigDecimal;
import java.math.RoundingMode;

public enum Desempenho {


    A_DESEJAR{
        public BigDecimal percentualReajuste(){
            return new BigDecimal("0.03");
        }
    },
    BOM{
        public BigDecimal percentualReajuste(){
            return new BigDecimal("0.15");
        }
    },
    OTIMO{
        public BigDecimal percentualReajuste(){
            return new BigDecimal("0.2");
        }
    },
    ESPETACULAR{
        public BigDecimal percentualReajuste(){
            return new BigDecimal("0.4");
        }
    };
    public abstract BigDecimal percentualReajuste();

}
